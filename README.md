## Kakuro ##
*Only Html and CSS were used to create this game.*
Inspired by student project. I attempt to make my own version of a games they have submitted using only HTML and CSS.

Kakuro is compared to being a crossword puzzle that uses numbers. Using only the numbers 1 through 9 you must solve the puzzle by having the sum of the numbers equal to the numbers square either above it or to the left. There is only 1 solution and using logic you can solve the puzzle.

---

## Extra files used ##
List of files besides out index.html and style.css

1. Google Fonts (_____) to make it look a little nicer
2. Generated the board using https://www.kakuro-online.com/generator
3. PHP script and Excel file used to help create the HTML for the game board instead of manually writing out every singel cell. (includes the EXCEL files used to plan layout of boards)

---

## JavaScript ##
If I had include JavScript, what would be different?

1. Easier to check for correct answers (maybe)
2. Possibility of tabbing to work in the direction you are solving (maybe)
3. Highlighting current clues so we know which sum we are trying to accomplish

---

## Future Tasks ##
Notes on possible options/functionality that could be added

1. Pencil marks - to know what possible values it could be? (might required js - but something to think about)
2. Create other boards and add "Play Next Level" button when you've completed the board (only allow next level if validation not used!?)